# recipie-app-api-proxy

NGINX Proxy app for our recipie API App

## usage

### Enviornment variables 

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Host name of the app to forward request to (default: 'app')
* 'APP_PORT' - Port of the app to forward request to (default: '9000')